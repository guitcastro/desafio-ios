//
//  AlamofireGithubClient.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import ObjectMapper

class AlamofireGithubClient : GithubClient {
    
    static let repositoryEndpoint = "https://api.github.com/search/repositories"
    static func pullRequestEndPoint(repository: Repository) -> String {
        return "https://api.github.com/repos/\(repository.userName)/\(repository.title)/pulls?state=all"
    }
    
    static let failToParseJsonError = NSError(domain: "JsonParser", code: 0, userInfo: [NSLocalizedDescriptionKey: "Fail to parse JSON"])

    
    let manager:SessionManager
    
    init(manager: SessionManager) {
        self.manager = manager
    }
    
    func getRepositories(page: Int) -> Observable<[Repository]> {
        let parameters: [String:Any] = ["q": "language:Java",
                                        "sort" : "stars",
                                        "page" : page]
        
        return self.manager.rx.json(.get, AlamofireGithubClient.repositoryEndpoint, parameters: parameters).map({ json  in
            guard let jsonDictonary = json as? [String: Any] else {
                throw AlamofireGithubClient.failToParseJsonError
            }
            guard let repositories =  Mapper<Repository>().mapArray(JSONObject: jsonDictonary["items"]) else {
                throw AlamofireGithubClient.failToParseJsonError
            }
            return repositories
        })
    }
    
    func getPullRequest(repository: Repository) -> Observable<[PullRequest]> {
        let endpoint = AlamofireGithubClient.pullRequestEndPoint(repository: repository)
        return self.manager.rx.json(.get, endpoint).map({ json  in
            guard let pulls =  Mapper<PullRequest>().mapArray(JSONObject: json) else {
                throw AlamofireGithubClient.failToParseJsonError
            }
            return pulls
        })
    }
    
}
