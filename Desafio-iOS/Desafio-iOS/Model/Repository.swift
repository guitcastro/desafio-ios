//
//  Repository.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation

struct Repository {
    
    let title:String
    let description:String
    let forks:Int
    let stars:Int
    let userName:String
    let userImage:String
    
}
