//
//  Repository+Mapper.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import ObjectMapper

extension PullRequest : Mappable {
    
    init?(map: Map) {
        guard let name:String = map["title"].value() else {
            return nil
        }
        guard let description:String = map["body"].value() else {
            return nil
        }
        guard let userName:String = map["user.login"].value() else {
            return nil
        }
        guard let avatar:String = map["user.avatar_url"].value() else {
            return nil
        }
        
        guard let status:String = map["state"].value() else {
            return nil
        }
        
        let state:Status = status == "open" ? Status.open : Status.closed
        
        guard let openedAt:String = map["created_at"].value() else {
            return nil
        }
        
        guard let htmlUrl:String = map["html_url"].value() else {
            return nil
        }
        
        guard let date = Date(iso8601String: openedAt) else {
            return nil
        }
        
        self.init(title: name, description: description, userName: userName, userImage: avatar, status: state, openedAt: date, htmlUrl: htmlUrl)
    }
    
    mutating func mapping(map: Map) {
    }
    
}

