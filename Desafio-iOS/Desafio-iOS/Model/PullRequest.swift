//
//  PullRequest.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation

struct PullRequest {
    
    enum Status {
        case open
        case closed
    }
    
    let title:String
    let description:String
    let userName:String
    let userImage:String
    let status:Status
    let openedAt:Date
    let htmlUrl:String
}
