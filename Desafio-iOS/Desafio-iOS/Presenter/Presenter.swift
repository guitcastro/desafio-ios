//
//  File.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation

protocol Presenter {
    func showLoading()
    func hideLoading()
    func displayAlert(title: String, message:String)
}
