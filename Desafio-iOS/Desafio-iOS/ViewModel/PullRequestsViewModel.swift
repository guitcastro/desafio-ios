//
//  PullRequestsViewModel.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import RxSwift
import ObservableArray_RxSwift

import Foundation

class PullRequestsViewModel {
    
    let bag = DisposeBag()
    
    let client:GithubClient
    let presenter:Presenter
    let repository: Repository
    
    let openedPullRequests:Variable<String> = Variable("0")
    let closedPullRequests:Variable<String> = Variable("0")
    let title:Variable<String>
    var items:ObservableArray<PullRequestCellViewModel> = []
    
    init(client: GithubClient, repository:Repository, presenter:Presenter) {
        self.client = client
        self.repository = repository
        self.presenter = presenter
        self.title = Variable(repository.title)
    }
    
    func loadContent() {
        client.getPullRequest(repository: self.repository)
            .do(onSubscribe: {
                self.presenter.showLoading()
            }, onDispose : {
                self.presenter.hideLoading()
            }).map { pulls in
                pulls.map( { PullRequestCellViewModel($0) })
            }.subscribe(onNext: {
                self.items.append(contentsOf: $0)
                let opened = $0.filter( { $0.pullRequest.status == .open }).count
                let closed = self.items.count - opened
                self.openedPullRequests.value = String(opened)
                self.closedPullRequests.value = String(closed)
                
            }, onError: { _ in
                self.presenter.displayAlert(title: "Desculpe", message: "Não foi possível carregar o conteúdo. Tente novamente mais tarde")
            }).addDisposableTo(self.bag)

    }
    
}
