//
//  RepositoryViewModel.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import ObservableArray_RxSwift
import RxSwift
import RxCocoa

class RepositoriesViewModel {

    let client:GithubClient
    let presenter:Presenter
    let wireframe:RootWireframe
    let bag:DisposeBag = DisposeBag()
    
    var items:ObservableArray<RepositoryCellViewModel> = []
    var page = 0

    init(client: GithubClient , presenter: Presenter, wireframe: RootWireframe) {
        self.client = client
        self.presenter = presenter
        self.wireframe = wireframe
    }
    
    func loadContent() {
        self.page += 1
        client.getRepositories(page: self.page)
            .do(onSubscribe: {
                self.presenter.showLoading()
            }, onDispose : {
                self.presenter.hideLoading()
            }).map { repositories in
                repositories.map({ RepositoryCellViewModel($0,  wireframe: self.wireframe) })
            }.subscribe(onNext: {
                self.items.append(contentsOf: $0)
            }, onError: { _ in 
                self.presenter.displayAlert(title: "Desculpe", message: "Não foi possível carregar o conteúdo. Tente novamente mais tarde")
            }).addDisposableTo(self.bag)
    }

    
}
