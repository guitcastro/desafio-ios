//
//  PullRequestCellViewModel.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireImage
import Alamofire
import RxAlamofire

class PullRequestCellViewModel {
    
    let pullRequest:PullRequest
    let title:Variable<String>
    let description:Variable<String>
    let userName:Variable<String>
    let image:Observable<UIImage>
    let statusColor:Variable<UIColor>
    let statusText:Variable<String>
    let dateText:Variable<String>
    
    init(_ pull: PullRequest) {
        self.pullRequest = pull
        self.title = Variable(pull.title)
        self.description = Variable(pull.description)
        self.userName = Variable(pull.userName)
        self.statusColor = Variable(pull.status == PullRequest.Status.open ? UIColor.green : UIColor.red)
        self.statusText = Variable(pull.status == .open ? "Open" : "Closed")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"
        
        
        self.dateText = Variable(dateFormatter.string(from: pull.openedAt))
        
        self.image = RxAlamofireImage.imageRequest(pull.userImage)
    }
    
    
}
