//
//  RepositoryCellViewModel.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireImage
import Alamofire
import RxAlamofire

class RepositoryCellViewModel {
    
    let wireframe:RootWireframe
    
    let title:Variable<String>
    let description:Variable<String>
    let userName:Variable<String>
    let stars:Variable<Int>
    let forks:Variable<Int>
    let image:Observable<UIImage>
    
    let repository:Repository
    
    init(_ repository: Repository,  wireframe: RootWireframe) {
        self.repository = repository
        self.wireframe = wireframe
        self.title = Variable(repository.title)
        self.description = Variable(repository.description)
        self.userName = Variable(repository.userName)
        
        
        self.stars = Variable(repository.stars)
        self.forks = Variable(repository.forks)
        self.image = RxAlamofireImage.imageRequest(repository.userImage)
    }
    
    func didSelectedRepository() {
        self.wireframe.presentPullRequestScreen(repository: self.repository)
    }
    
}
