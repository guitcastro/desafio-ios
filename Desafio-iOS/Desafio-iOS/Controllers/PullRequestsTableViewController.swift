//
//  PullRequestsTableViewController.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import UIKit
import RxSwift

class PullRequestsTableViewController: UITableViewController {

    @IBOutlet weak var openedPullRequests: UILabel!
    @IBOutlet weak var closedPullRequests: UILabel!
    
    let bag = DisposeBag()

    // injected properties
    var repository: Repository!
    var client:GithubClient!
    
    var viewModel: PullRequestsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = nil
        self.tableView.dataSource = nil
        
        self.viewModel = PullRequestsViewModel(client: self.client, repository: self.repository, presenter: self)
        
        self.viewModel.openedPullRequests.asObservable().bindTo(self.openedPullRequests.rx.text).addDisposableTo(self.bag)
        self.viewModel.closedPullRequests.asObservable().bindTo(self.closedPullRequests.rx.text).addDisposableTo(self.bag)
        self.viewModel.title.asObservable().bindTo(self.rx.title).addDisposableTo(self.bag)
        
        self.tableView.rx.modelSelected(PullRequestCellViewModel.self).subscribe { (viewModel) in
            if let url = URL(string: viewModel.element!.pullRequest.htmlUrl) {
                UIApplication.shared.openURL(url)
            }
        }.addDisposableTo(self.bag)
        
        
        
        
        self.viewModel.items.rx_elements().bindTo(self.tableView.rx.items(cellIdentifier: "Cell", cellType: PullRequestCellView.self)){ (row, element, cell) in
            element.title.asObservable().bindTo(cell.title.rx.text).addDisposableTo(self.bag)
            element.statusColor.asObservable().bindTo(cell.openLabel.rx.textColor).addDisposableTo(self.bag)
            element.statusText.asObservable().bindTo(cell.openLabel.rx.text).addDisposableTo(self.bag)
            element.dateText.asObservable().bindTo(cell.dateLabel.rx.text).addDisposableTo(self.bag)
            element.description.asObservable().bindTo(cell.body.rx.text).addDisposableTo(self.bag)
            element.userName.asObservable().bindTo(cell.userName.rx.text).addDisposableTo(self.bag)
            element.image.bindTo(cell.userPhoto.rx.image).addDisposableTo(self.bag)
            
            }.addDisposableTo(self.bag)
        
        self.viewModel.loadContent()
    }

   

}
