//
//  RepositoriesTableViewController.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RepositoriesTableViewController: UITableViewController {
    
    let bag = DisposeBag()
    var viewModel:RepositoriesViewModel!
    
    // Injected properties
    var githubClient:GithubClient!
    var rootWireframe:RootWireframe!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Github JavaPop"
        
        self.tableView.dataSource = nil
        self.tableView.delegate = nil
        
        self.viewModel = RepositoriesViewModel(client: self.githubClient, presenter: self, wireframe: self.rootWireframe)
        
        self.tableView.rx.willDisplayCell.subscribe { (event) in
            let row:Int? = event.element?.indexPath.row
            let count:Int? = self.viewModel.items.count - 1
            if row == count {
                self.viewModel.loadContent()
            }
        }.addDisposableTo(self.bag)
        
        self.tableView.rx.modelSelected(RepositoryCellViewModel.self).subscribe { (repositoryViewModel) in
            repositoryViewModel.element?.didSelectedRepository()
        }.addDisposableTo(self.bag)
        
        
        self.viewModel.items.rx_elements().bindTo(self.tableView.rx.items(cellIdentifier: "Cell", cellType: RepositoryTableViewCell.self)){ (row, element, cell) in
            element.title.asObservable().bindTo(cell.title.rx.text).addDisposableTo(self.bag)
            element.description.asObservable().bindTo(cell.repositoryDerscription.rx.text).addDisposableTo(self.bag)
            element.forks.asObservable().map({ String($0) }).bindTo(cell.forks.rx.text).addDisposableTo(self.bag)
            element.stars.asObservable().map({ String($0) }).bindTo(cell.stars.rx.text).addDisposableTo(self.bag)
            element.userName.asObservable().bindTo(cell.userName.rx.text).addDisposableTo(self.bag)
            element.image.bindTo(cell.userPhoto.rx.image).addDisposableTo(self.bag)
            
        }.addDisposableTo(self.bag)
        
        self.viewModel.loadContent()
        
    }
    
    
}
