//
//  UIViewController+Presenter.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

extension UIViewController : Presenter {
    
    func showLoading() {
        HUD.show(.progress)
    }
    func hideLoading() {
        HUD.hide()
    }
    
    func displayAlert(title: String, message:String) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
}
