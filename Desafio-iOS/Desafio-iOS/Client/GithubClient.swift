//
//  GithubClient.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import RxSwift

protocol GithubClient {
    
    func getRepositories(page: Int) -> Observable<[Repository]>
    func getPullRequest(repository: Repository) -> Observable<[PullRequest]>
    
    
}
