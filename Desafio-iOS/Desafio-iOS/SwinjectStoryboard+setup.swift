//
//  SwinjectStoryboard+setup.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import SwinjectStoryboard
import Swinject
import Alamofire

extension SwinjectStoryboard {
    class func setup() {
        
        defaultContainer.register(GithubClient.self) { _ in AlamofireGithubClient(manager: SessionManager()) }
        
        defaultContainer.storyboardInitCompleted(UINavigationController.self) { r, c in
            defaultContainer.register(RootWireframe.self) { _ in
                RootWireframe(navigationController: c)
            }
        }
        
        defaultContainer.storyboardInitCompleted(RepositoriesTableViewController.self) { r, c in
            let client = r.resolve(GithubClient.self)!
            c.githubClient = client
            c.rootWireframe = r.resolve(RootWireframe.self)
        }
        
        defaultContainer.storyboardInitCompleted(PullRequestsTableViewController.self) { r, c in
            let client = r.resolve(GithubClient.self)!
            c.client = client
        }
    }
}
