//
//  Repository+Mapper.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import ObjectMapper

extension Repository : Mappable {
    
    init?(map: Map) {
        guard let name:String = map["name"].value() else {
            return nil
        }
        guard let description:String = map["description"].value() else {
            return nil
        }
        guard let userName:String = map["owner.login"].value() else {
            return nil
        }
        guard let stars:Int = map["stargazers_count"].value() else {
            return nil
        }
        guard let forks:Int = map["forks"].value() else {
            return nil
        }
        guard let avatar:String = map["owner.avatar_url"].value() else {
            return nil
        }
        self.init(title: name, description: description, forks: forks, stars: stars, userName: userName, userImage: avatar)
    }
    
    mutating func mapping(map: Map) {
    }
    
}

