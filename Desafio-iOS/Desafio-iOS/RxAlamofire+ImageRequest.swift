//
//  RxAlamofire+ImageRequest.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import UIKit

class RxAlamofireImage {
    
    class func imageRequest(_ url:String) -> Observable<UIImage> {
        return RxAlamofire.request(.get, url).flatMap { (request)  -> Observable<UIImage> in
            return Observable.create({ (observer) -> Disposable in
                request.responseImage(completionHandler: { (imageResponse) in
                    if let image = imageResponse.result.value?.af_imageRoundedIntoCircle() {
                        observer.on(.next(image))
                        observer.on(.completed)
                    } else {
                        let error = NSError(domain: "Image Downloaded", code: 0, userInfo: [NSLocalizedDescriptionKey: "Fail to download image"])
                        observer.on(.error(error))
                    }
                })
                return Disposables.create()
            })
        }

    }
    
}
