//
//  RootWireframe.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import UIKit

class RootWireframe {
    
    let navigationController:UINavigationController
    
    init(navigationController:UINavigationController) {
        self.navigationController = navigationController
    }
    
    func presentPullRequestScreen(repository: Repository) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PullRequestsTableViewController") as! PullRequestsTableViewController
        controller.repository = repository
        
        self.navigationController.pushViewController(controller, animated: true)
    }
    
}
