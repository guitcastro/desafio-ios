//
//  RepositoryTableViewCell.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var repositoryDerscription: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
