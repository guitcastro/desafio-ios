//
//  UILabel+RxTextColor.swift
//  Desafio-iOS
//
//  Created by Guilherme on 11/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit
    
extension Reactive where Base: UILabel {
    
    /// Bindable sink for `text` property.
    public var textColor: UIBindingObserver<Base, UIColor> {
        return UIBindingObserver(UIElement: self.base) { label, color in
            label.textColor = color
        }
    }
    
}
