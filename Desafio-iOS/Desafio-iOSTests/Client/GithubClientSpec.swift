//
//  GithubClient.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Alamofire
import OHHTTPStubs

@testable import Desafio_iOS


class GithubClientSpec:QuickSpec {
    
    var subject: GithubClient?
    
    override func spec() {
        describe("The github Client") {
            beforeEach {
                self.subject = AlamofireGithubClient(manager: SessionManager())
            }
            
            context("Success get repositories") {
                
                beforeEach {
                    self.mockSuccessRequest()
                }
                
                it("receveid an array of repositories") {
                    var repositories = [Repository]()
                    _ = self.subject?.getRepositories(page: 0).subscribe(onNext: { response in
                        repositories = response
                    }, onError: {error in
                        print(error.localizedDescription)
                    })
                    expect{repositories}.toEventuallyNot(beEmpty())
                    
                }
            }
            
            context("Success get pulls") {
                
                beforeEach {
                    self.mockSuccessGetPullsRequest()
                }
                
                it("receveid an array of pull requests") {
                    var pulls = [PullRequest]()
                    let repository = Repository(title: "test", description: "test", forks: 1, stars: 1, userName: "test", userImage: "")
                    _ = self.subject?.getPullRequest(repository: repository).subscribe(onNext: { response in
                        pulls = response
                    }, onError: {error in
                        print(error.localizedDescription)
                    })
                    expect{pulls}.toEventuallyNot(beEmpty())
                    
                }
            }
        }
    }
    
    func mockSuccessRequest() {
        stub(condition: isHost("api.github.com")) { _ in
            let stubPath = OHPathForFile("repositories.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type" as NSObject:"application/json" as AnyObject])
        }
    }
    
    
    func mockSuccessGetPullsRequest() {
        stub(condition: isHost("api.github.com")) { _ in
            let stubPath = OHPathForFile("pulls.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type" as NSObject:"application/json" as AnyObject])
        }
    }

}
