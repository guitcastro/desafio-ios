//
//  GithubClient.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Alamofire
import OHHTTPStubs
import RxSwift

@testable import Desafio_iOS


class RepositoriesViewModelSpec:QuickSpec {
    
    var mockClient:MockGithubClient!
    var mockPresenter: MockPresenter!
    var subject: RepositoriesViewModel?
    
    override func spec() {
        describe("The Repositories ViewModel") {
            beforeEach {
                self.mockClient = MockGithubClient()
                self.mockPresenter = MockPresenter()
                self.subject = RepositoriesViewModel(client: self.mockClient, presenter: self.mockPresenter,
                                                     wireframe: RootWireframe(navigationController: UINavigationController()))
            }
            
            context("Success get repositories") {
                
                beforeEach {
                    self.mockSuccessRequest()
                    self.subject?.loadContent()
                }
                
                it("Call show loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Call hide loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Did not call show alert function") {
                    expect{self.mockPresenter.displayAlertCallCounter}.to(equal(0))
                }
            }
            
            context("Fail get repositories") {
                
                beforeEach {
                    self.subject?.loadContent()
                }
                
                it("Call show loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Call hide loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Call show alert function") {
                    expect{self.mockPresenter.displayAlertCallCounter}.to(equal(1))
                }
            }
        }
    }
    
    func mockSuccessRequest() {
        let repository = Repository(title: "test", description: "test", forks: 3, stars: 3, userName: "guitcastro", userImage: "http://example.com")
        self.mockClient.repositoriesClosure = Observable.just([repository])
    }
    
    
    class MockGithubClient : GithubClient {
        var repositoriesClosure:Observable<[Repository]>?
        
        func getRepositories(page: Int) -> Observable<[Repository]> {
            return repositoriesClosure ?? Observable.error(NSError(domain: "test", code: 0, userInfo: nil))
        }
        
        func getPullRequest(repository: Repository) -> Observable<[PullRequest]>{
            return Observable.empty()
        }
        
    }
    
    class MockPresenter : Presenter {
        
        var showLoadingCallCounter = 0
        var hideLoadingCallCounter = 0
        var displayAlertCallCounter = 0
        
        func showLoading() {
            showLoadingCallCounter += 1
        }
        
        func hideLoading() {
            hideLoadingCallCounter += 1
        }
        
        func displayAlert(title: String, message:String) {
            displayAlertCallCounter += 1
        }
        
    }
    
}
