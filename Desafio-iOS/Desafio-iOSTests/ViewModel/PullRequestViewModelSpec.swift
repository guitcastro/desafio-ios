//
//  GithubClient.swift
//  Desafio-iOS
//
//  Created by Guilherme on 10/01/17.
//  Copyright © 2017 Guilherme. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Alamofire
import OHHTTPStubs
import RxSwift

@testable import Desafio_iOS


class PullRequestViewModelSpec:QuickSpec {
    
    var mockClient:MockGithubClient!
    var mockPresenter: MockPresenter!
    var subject: PullRequestsViewModel?
    
    override func spec() {
        describe("The Pull requests ViewModel") {
            beforeEach {
                self.mockClient = MockGithubClient()
                self.mockPresenter = MockPresenter()
                let repository = Repository(title: "test", description: "test", forks: 1, stars: 1, userName: "test", userImage: "test")
                self.subject = PullRequestsViewModel(client: self.mockClient,repository: repository,  presenter: self.mockPresenter)
            }
            
            context("Success get repositories") {
                
                beforeEach {
                    self.mockSuccessRequest()
                    self.subject?.loadContent()
                }
                
                it("Call show loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Call hide loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Did not call show alert function") {
                    expect{self.mockPresenter.displayAlertCallCounter}.to(equal(0))
                }
            }
            
            context("Fail get repositories") {
                
                beforeEach {
                    self.subject?.loadContent()
                }
                
                it("Call show loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Call hide loading function") {
                    expect{self.mockPresenter.showLoadingCallCounter}.to(equal(1))
                }
                it("Call show alert function") {
                    expect{self.mockPresenter.displayAlertCallCounter}.to(equal(1))
                }
            }
        }
    }
    
    func mockSuccessRequest() {
        let pullRequest = PullRequest(title: "test", description: "test", userName: "test", userImage: "test", status: .open, openedAt: Date(), htmlUrl: "")
        self.mockClient.pullRequests = Observable.just([pullRequest])
    }
    
    
    class MockGithubClient : GithubClient {
        var pullRequests:Observable<[PullRequest]>?
        
        func getRepositories(page: Int) -> Observable<[Repository]> {
            return Observable.empty()
        }
        
        func getPullRequest(repository: Repository) -> Observable<[PullRequest]>{
            return pullRequests ?? Observable.error(NSError(domain: "test", code: 0, userInfo: nil))
        }
        
    }
    
    class MockPresenter : Presenter {
        
        var showLoadingCallCounter = 0
        var hideLoadingCallCounter = 0
        var displayAlertCallCounter = 0
        
        func showLoading() {
            showLoadingCallCounter += 1
        }
        
        func hideLoading() {
            hideLoadingCallCounter += 1
        }
        
        func displayAlert(title: String, message:String) {
            displayAlertCallCounter += 1
        }
        
    }
    
}
